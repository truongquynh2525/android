package com.example.thiguaky.src.Controller.Librarian;

import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class LibrarianActivity extends AppCompatActivity {
    public static void insert(Connection conn, String libName, String sdt, String username, String password) {
        String sql = "INSERT INTO ThuThu VALUES (? ,? ,? ,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, libName);
            stmt.setString(2, sdt);
            stmt.setString(3, username);
            stmt.setString(4, password);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public void update(Connection conn, int libID, String libName, String sdt, String username, String password) {
        String sql = "UPDATE ThuThu SET LibName = ?, SDT = ?, Username = ?, Password = ? WHERE LibID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, libName);
            stmt.setString(2, sdt);
            stmt.setString(3, username);
            stmt.setString(4, password);
            stmt.setInt(5, libID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public void delete(Connection conn, String query, int libID) {
        String sql = "DELETE FROM ThuThu WHERE LibID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, libID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static int checkLogin(Connection con, String username, String password) {
        int libID = 0;
        try {
            Connection conn = con;
            Statement stmt = conn.createStatement();
            String sql = "SELECT LibID FROM ThuThu WHERE Username='" + username + "' AND Password='" + password + "'";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                libID = rs.getInt(1);
            }
            return libID;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return libID;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_librarian);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Danh sách thủ thư"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
    }
}