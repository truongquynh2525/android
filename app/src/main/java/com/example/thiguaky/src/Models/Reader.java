package com.example.thiguaky.src.Models;

import java.sql.Date;

public class Reader {
    private int ReaderID;
    private String ReaderHo;
    private String ReaderName;
    private Date BirthDay;
    private int Sex;
    private String Address;

    public Reader() {

    }

    public Reader(int readerID, String readerHo, String readerName, Date birthDay, int sex, String address) {
        ReaderID = readerID;
        ReaderHo = readerHo;
        ReaderName = readerName;
        BirthDay = birthDay;
        Sex = sex;
        Address = address;
    }

    public int getReaderID() {
        return ReaderID;
    }

    public void setReaderID(int readerID) {
        ReaderID = readerID;
    }

    public String getReaderName() {
        return ReaderName;
    }

    public void setReaderName(String readerName) {
        ReaderName = readerName;
    }

    public Date getBirthDay() {
        return BirthDay;
    }

    public void setBirthDay(Date birthDay) {
        BirthDay = birthDay;
    }

    public int getSex() {
        return Sex;
    }

    public String getReaderHo() {
        return ReaderHo;
    }

    public void setReaderHo(String readerHo) {
        ReaderHo = readerHo;
    }

    public void setSex(int sex) {
        Sex = sex;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    @Override
    public String toString() {
        return ReaderHo + ' ' + ReaderName;
    }
}
