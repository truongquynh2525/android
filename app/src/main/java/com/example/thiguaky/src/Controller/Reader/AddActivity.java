package com.example.thiguaky.src.Controller.Reader;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;

import java.sql.Connection;
import java.sql.Date;

public class AddActivity extends AppCompatActivity {
    ConnectToDB con;
    Connection conn;

    Button btnAccept, btnExit;
    EditText readerHo, readerName, diaChi;
    CheckBox chxNam, chxNu;
    int sex;
    DatePicker date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_reader);
        con = new ConnectToDB();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Add Reader"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
      //  actionBar.setLogo(R.drawable.readerr);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
        setEvent();
    }

    private void setControl() {
        readerHo = (EditText) findViewById(R.id.readerHo);
        readerName = (EditText) findViewById(R.id.readerName);
        diaChi = (EditText) findViewById(R.id.readerAddress);
        date = (DatePicker) findViewById(R.id.date);
        chxNam = (CheckBox) findViewById(R.id.chxNam);
        chxNu = (CheckBox) findViewById(R.id.chxNu);
        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnExit = (Button) findViewById(R.id.btnExit);
    }

    private void setEvent() {
        sex = 1;
        if(chxNu.isChecked()) {
            sex = 0;
            chxNam.setChecked(false);
            chxNu.setChecked(true);
        }
        else {
            sex = 1;
            chxNu.setChecked(false);
            chxNam.setChecked(true);
        }
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn = con.conclass();
                long dateTime = date.getCalendarView().getDate();
                Date time = new Date(dateTime);
                ReaderActivity.insert(conn, readerHo.getText().toString(), readerName.getText().toString(), time, sex, diaChi.getText().toString());
                Toast.makeText(AddActivity.this, "Thêm đọc giả thành công", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                startActivity(intent);
                try {
                    conn.close();
                }catch (Exception ex) {

                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                startActivity(intent);
            }
        });
    }
}