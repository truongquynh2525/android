package com.example.thiguaky.src.Controller.ReturnBook;

import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Reader;
import com.example.thiguaky.src.Models.ReturnBook;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Controller {
    public static Connection conn=null;

    public Controller()
    {
        conn=new ConnectToDB().conclass();
    }

    public static void updateBorrowBook(int status, int ticketID, int bookID) {
        String sql = "UPDATE [dbo].[CTPM] SET Status = ? WHERE TicketID = ? AND BookID=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, status);
            stmt.setInt(2, ticketID);
            stmt.setInt(3, bookID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static ArrayList<ReturnBook> loadData(int maDocGia) {
        ArrayList<ReturnBook> arrayList=new ArrayList<>();
        String sql = "EXEC [dbo].[loadListBorrowBook] @MaDocGia ="+maDocGia;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ReturnBook returnBook=new ReturnBook(rs.getInt("BookID"),rs.getInt("TicketID"),
                        rs.getString("name"),rs.getString("img"),rs.getDate("Times"));
                arrayList.add(returnBook);

            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return  arrayList;
    }

    public static ArrayList<Reader> loadListReaderBorrow() {
        ArrayList<Reader> arrayList=new ArrayList<>();
        String sql = "SELECT DISTINCT DocGia.ReaderID,DocGia.ReaderHo,DocGia.ReaderTen,DocGia.BirthDay,DocGia.Sex,DocGia.Address FROM DocGia\n" +
                "\tJOIN PhieuMuon ON DocGia.ReaderID=PhieuMuon.ReaderID\n" +
                "\tJOIN CTPM ON PhieuMuon.TicketID=CTPM.TicketID\n" +
                "WHERE Status=0";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reader reader=new Reader(rs.getInt("ReaderID"),rs.getString("ReaderHo"),
                        rs.getString("ReaderTen"),rs.getDate("BirthDay"),
                        rs.getInt("Sex"),rs.getString("Address"));

                arrayList.add(reader);
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return arrayList;
    }

    public static ArrayList<Reader> filter(String query){
        ArrayList<Reader> arrayList=new ArrayList<>();
        String sql = "SELECT DISTINCT DocGia.ReaderID,DocGia.ReaderHo,DocGia.ReaderTen,DocGia.BirthDay,DocGia.Sex,DocGia.Address FROM DocGia\n" +
                "                JOIN PhieuMuon ON DocGia.ReaderID=PhieuMuon.ReaderID\n" +
                "                JOIN CTPM ON PhieuMuon.TicketID=CTPM.TicketID\n" +
                "                WHERE Status=0 AND DocGia.ReaderHo +' '+ DocGia.ReaderTen LIKE '%"+query+"%'";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Reader reader=new Reader(rs.getInt("ReaderID"),rs.getString("ReaderHo"),
                        rs.getString("ReaderTen"),rs.getDate("BirthDay"),
                        rs.getInt("Sex"),rs.getString("Address"));

                arrayList.add(reader);
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return arrayList;
    }
}
