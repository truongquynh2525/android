package com.example.thiguaky.src.Controller.ReturnBook;

import android.os.Bundle;
import android.widget.GridView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.ReturnBook.Custom.CustomReturnBookAdapter;
import com.example.thiguaky.src.Models.ReturnBook;

import java.util.ArrayList;

public class ReturnActivity extends AppCompatActivity {
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_book);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay Lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
    }

    private void setControl()
    {
        this.gridView = (GridView)findViewById(R.id.gridView);

        int readerId=1;
        String session = getIntent().getStringExtra("READER_ID");
        if(session!=null)
        {
            readerId = Integer.parseInt(session);
        }

        ArrayList<ReturnBook> arrayListBook= Controller.loadData(readerId);

        CustomReturnBookAdapter adapter=new CustomReturnBookAdapter(this,R.layout.layout_return_book,arrayListBook);
        gridView.setAdapter(adapter);
    }
}
