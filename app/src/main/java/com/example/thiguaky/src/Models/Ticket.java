package com.example.thiguaky.src.Models;

import java.sql.Date;

public class Ticket {
    private int TicketID;
    private int LibID;
    private int ReaderID;
    private Date Times;
    private String Notes;

    public int getTicketID() {
        return TicketID;
    }

    public void setTicketID(int ticketID) {
        TicketID = ticketID;
    }

    public int getLibID() {
        return LibID;
    }

    public void setLibID(int libID) {
        LibID = libID;
    }

    public int getReaderID() {
        return ReaderID;
    }

    public void setReaderID(int readerID) {
        ReaderID = readerID;
    }

    public Date getTimes() {
        return Times;
    }

    public void setTimes(Date times) {
        Times = times;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }
}
