package com.example.thiguaky.src.Controller.Statistic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;

public class MainActivity extends AppCompatActivity {
    Button btn1,btn2,btn3;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic_book);
        View v = (View)findViewById(R.id.statisticImg);
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
        v.setAnimation(animScale);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Thống kê"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
    }

    private void setControl()
    {
        new Controller();
        this.btn1=(Button)findViewById(R.id.btn1);
        this.btn2=(Button)findViewById(R.id.btn2);
        this.btn3=(Button)findViewById(R.id.btn3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), BorrowBookMaxActivity.class);
                startActivity(intent);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CombinedChartActivity.class);
                startActivity(intent);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), CombinedChart1Activity.class);
                startActivity(intent);
            }
        });
    }
}
