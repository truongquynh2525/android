package com.example.thiguaky.src.Controller.Librarian;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.LoginActivity;

import java.sql.Connection;

public class AddActivity extends AppCompatActivity {
    Button btnAccept, btnExit;
    EditText libName, sdt, username, password;

    ConnectToDB con;
    Connection conn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_librarian);
        con = new ConnectToDB();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
        setEvent();
    }

    private void setControl() {
        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnExit = (Button) findViewById(R.id.btnExit);
        libName = (EditText) findViewById(R.id.libName);
        sdt = (EditText) findViewById(R.id.sdt);
        username = (EditText) findViewById(R.id.Username);
        password = (EditText) findViewById(R.id.Password);
    }

    private void setEvent() {
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn = con.conclass();
                LibrarianActivity.insert(conn, libName.getText().toString(), sdt.getText().toString(), username.getText().toString(), password.getText().toString());
                Toast.makeText(AddActivity.this, "Đăng ký thành công", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(v.getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}