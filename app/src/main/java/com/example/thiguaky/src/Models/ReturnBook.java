package com.example.thiguaky.src.Models;

import java.sql.Date;

public class ReturnBook {
    private int idBook;
    private int idBorrow;
    private String name;
    private String img;
    private Date date;

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public int getIdBorrow() {
        return idBorrow;
    }

    public void setIdBorrow(int idBorrow) {
        this.idBorrow = idBorrow;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ReturnBook() {

    }

    public ReturnBook(int idBook, int idBorrow, String name, String img,Date date) {
        this.idBook = idBook;
        this.idBorrow = idBorrow;
        this.name = name;
        this.img = img;
        this.date=date;
    }
}
