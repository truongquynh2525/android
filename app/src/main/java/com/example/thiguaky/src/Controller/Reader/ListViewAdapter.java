package com.example.thiguaky.src.Controller.Reader;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Models.Reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    private List<Reader> readerList = null;
    private ArrayList<Reader> arraylist;

    public ListViewAdapter(Context context, List<Reader> animalNamesList) {
        mContext = context;
        this.readerList = animalNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Reader>();
        this.arraylist.addAll(animalNamesList);
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return readerList.size();
    }

    @Override
    public Reader getItem(int position) {
        return readerList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.reader_search, null);
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        System.out.println("view");
        // Set the results into TextViews
        holder.name.setText("Họ và tên: " + readerList.get(position).getReaderHo() + " " + readerList.get(position).getReaderName());
        return view;
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        readerList.clear();
        if (charText.length() == 0) {
            readerList.addAll(arraylist);
        } else {
            for (Reader wp : arraylist) {
                if (wp.getReaderName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    readerList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
