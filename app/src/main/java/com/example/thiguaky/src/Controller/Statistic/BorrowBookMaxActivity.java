package com.example.thiguaky.src.Controller.Statistic;

import android.os.Bundle;
import android.widget.GridView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.Statistic.Custom.CustomBookStatisticAdapter;
import com.example.thiguaky.src.Models.Book;

import java.util.ArrayList;

public class BorrowBookMaxActivity extends AppCompatActivity {
    GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic_borrow_book_max);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Sách mượn nhiều nhất"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
    }

    private void setControl()
    {
        this.gridView = (GridView)findViewById(R.id.gridView);

        ArrayList<Book> arrayListBook= Controller.statisticBookBorrowMax();

        CustomBookStatisticAdapter adapter=new CustomBookStatisticAdapter(this,R.layout.layout_statistic_borrow_book,arrayListBook);
        gridView.setAdapter(adapter);
    }
}
