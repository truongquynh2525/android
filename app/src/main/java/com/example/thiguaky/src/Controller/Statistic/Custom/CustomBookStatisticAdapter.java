package com.example.thiguaky.src.Controller.Statistic.Custom;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Models.Book;

import java.util.ArrayList;

public class CustomBookStatisticAdapter extends ArrayAdapter {
    Context context;
    ArrayList<Book> bookList=new ArrayList<>();
    int layoutID;

    public CustomBookStatisticAdapter(@NonNull Context context, int layoutID, ArrayList<Book> bookList) {
        super(context, layoutID);
        this.context = context;
        this.bookList = bookList;
        this.layoutID = layoutID;
    }

    @Override
    public int getCount()
    {
        return bookList.size();
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater.from(context));
        convertView = inflater.inflate(layoutID, null);

        TextView stt = (TextView) convertView.findViewById(R.id.text_stt_book_item);
        TextView name = (TextView) convertView.findViewById(R.id.text_name_book_item);
        TextView count = (TextView) convertView.findViewById(R.id.text_count_book_item);
        ImageView img = (ImageView) convertView.findViewById(R.id.text_img_book_item);

        String imgUrl=bookList.get(positon).getImg();

        stt.setText(bookList.get(positon).getId() + "");
        name.setText(bookList.get(positon).getName());
        count.setText(Math.round(bookList.get(positon).getPrice())+"");

        try {
            img.setImageURI(Uri.parse(bookList.get(positon).getImg()));
            if(img.getDrawable() == null)
            {
                img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
            }
        }catch (Exception exception)
        {
            img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
        }

        return convertView;
    }
}
