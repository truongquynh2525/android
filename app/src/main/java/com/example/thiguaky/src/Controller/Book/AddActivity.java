package com.example.thiguaky.src.Controller.Book;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Models.Book;

public class AddActivity extends AppCompatActivity {
    EditText editTextName, editTextPrice;
    Button btnXacNhan,btnCancel;
    ImageView imageView;
    FileChooserFragment fragment;
    int bookId=-1;
    Integer[] listImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
        setEvent();
    }

    private void setControl() {
        editTextName =(EditText) findViewById(R.id.bookName);
        editTextPrice=(EditText) findViewById(R.id.bookPrice);
        imageView=(ImageView) findViewById(R.id.bookImg);
        editTextPrice.setInputType(InputType.TYPE_CLASS_NUMBER );

        FragmentManager fragmentManager = this.getSupportFragmentManager();
        this.fragment = (FileChooserFragment) fragmentManager.findFragmentById(R.id.fragment_fileChooser);
        this.fragment.setImageView(imageView);

        btnXacNhan=(Button)findViewById(R.id.btnAccept);
        btnCancel=(Button)findViewById(R.id.btnExit);

        String session = getIntent().getStringExtra("BOOK_ID");
        if(session!=null)
        {
            bookId = Integer.parseInt(session);
        }

        if(bookId!=-1)
        {
            Book book= Controller.get(bookId);
            editTextName.setText(book.getName().toString());
            editTextPrice.setText(book.getPrice().toString());

            try {
                imageView.setImageURI(Uri.parse(book.getImg()));
                if(imageView.getDrawable() == null)
                {
                    imageView.setImageResource(R.drawable.book);
                }
            } catch (Exception exception)
            {
                imageView.setImageResource(R.drawable.book);
            }

            fragment.setPath(book.getImg().toString());
        }
    }

    private void setEvent()
    {
        btnXacNhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(vaildate(editTextName.getText().toString(),editTextPrice.getText().toString(),fragment.getPath()))
                {
                    String name= editTextName.getText().toString();
                    Float price=Float.parseFloat(editTextPrice.getText().toString());
                    String imgURL=fragment.getPath();

                    if(bookId != -1)
                    {
                        Controller.update(bookId,name,price,imgURL);
                    }
                    else
                    {
                        Controller.insert(name,price,imgURL);
                    }

                    Toast.makeText(getApplication(),"Thành công",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(v.getContext(), MainActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    public boolean vaildate(String name,String price,String imgURL)
    {
        if(name.equals(""))
        {
            Toast.makeText(getApplication(),"Vui lòng nhập tên",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(price.equals(""))
        {
            Toast.makeText(getApplication(),"Vui lòng nhập giá",Toast.LENGTH_LONG).show();
            return false;
        }
        else if(imgURL.equals(""))
        {
            Toast.makeText(getApplication(),"Vui lòng chọn ảnh",Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}