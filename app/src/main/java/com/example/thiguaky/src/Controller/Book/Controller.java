package com.example.thiguaky.src.Controller.Book;

import android.util.Log;

import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Ticket;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Controller {
    public static Connection conn=null;

    public Controller()
    {
        conn=new ConnectToDB().conclass();
    }

    public static void insert(String name, Float price, String img) {
        String sql = "INSERT INTO Sach (name, price, img) VALUES (?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setFloat(2, price);
            stmt.setString(3, img);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void update(int ID, String name, Float price, String img) {
        String sql = "UPDATE Sach SET name = ?, price = ?, img = ? WHERE id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            stmt.setFloat(2, price);
            stmt.setString(3, img);
            stmt.setInt(4, ID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void updateBorrowBook(int status, int ticketID, int bookID) {
        String sql = "UPDATE [dbo].[CTPM] SET Status = ? WHERE TicketID = ? AND BookID=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, status);
            stmt.setInt(2, ticketID);
            stmt.setInt(3, bookID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void delete(int ID) {
        String sql = "DELETE FROM Sach WHERE id = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static ArrayList<Book> loadData() {
        ArrayList<Book> arrayList=new ArrayList<>();
        String sql = "SELECT * FROM Sach";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Book book=new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getFloat("price"),rs.getString("img"));
                arrayList.add(book);
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return  arrayList;
    }

    public static Book get(int id) {
        Book book=null;

        String sql = "SELECT * FROM Sach WHERE id="+id;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                book=new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getFloat("price"),rs.getString("img"));
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return book;
    }

    public static boolean ktXoa(int id) {
        int n=0;

        String sql = "SELECT * FROM CTPM WHERE CTPM.BookID="+id;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                n = 1;
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        if(n==0)
        {
            return true;
        }
        return false;
    }

    public static ArrayList<Book> filter(String query){
        ArrayList<Book> arrayList = new ArrayList<>();
        String sql = "SELECT * FROM Sach WHERE name LIKE '%"+query+"%'";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Book book=new Book(rs.getInt("id"),rs.getString("name"),
                        rs.getFloat("price"),rs.getString("img"));
                arrayList.add(book);
            }
            rs.close();
            ps.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }

        return arrayList;
    }
}
