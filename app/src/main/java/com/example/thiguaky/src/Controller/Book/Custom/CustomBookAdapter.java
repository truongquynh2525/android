package com.example.thiguaky.src.Controller.Book.Custom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.Book.AddActivity;
import com.example.thiguaky.src.Controller.Book.MainActivity;
import com.example.thiguaky.src.Controller.Book.Controller;
import com.example.thiguaky.src.Controller.LoginActivity;
import com.example.thiguaky.src.Models.Book;

import java.util.ArrayList;

public class CustomBookAdapter extends ArrayAdapter {
    Context context;
    ArrayList<Book> bookList=new ArrayList<>();
    int layoutID;

    public CustomBookAdapter(@NonNull Context context, int layoutID, ArrayList<Book> bookList) {
        super(context, layoutID);
        this.context = context;
        this.bookList = bookList;
        this.layoutID = layoutID;
    }

    public ArrayList<Book> getBookList() {
        return bookList;
    }

    public void setBookList(ArrayList<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public int getCount()
    {
        return bookList.size();
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=(LayoutInflater.from(context));
        convertView=inflater.inflate(layoutID,null);
        TextView id=(TextView)convertView.findViewById(R.id.text_id_book_item);
        TextView name=(TextView) convertView.findViewById(R.id.text_name_book_item);
        TextView price=(TextView)convertView.findViewById(R.id.text_price_book_item);
        ImageView img=(ImageView) convertView.findViewById(R.id.text_img_book_item);
        Button btnEdit=(Button) convertView.findViewById(R.id.btnEdit);
        Button btnDelete=(Button) convertView.findViewById(R.id.btnDelete);

        id.setText(bookList.get(positon).getId()+"");
        name.setText(bookList.get(positon).getName());
        price.setText(bookList.get(positon).getPrice().toString());

        try {
            img.setImageURI(Uri.parse(bookList.get(positon).getImg()));
            if(img.getDrawable() == null)
            {
                img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
            }
        }catch (Exception exception)
        {
            img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200, 200);
        layoutParams.setMargins(0, 0, 0, 0);
        img.setLayoutParams(layoutParams);

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddActivity.class);
                intent.putExtra("BOOK_ID", bookList.get(positon).getId()+"");
                context.startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder b = new AlertDialog.Builder(context);

                b.setTitle("Xác nhận");
                b.setMessage("Bạn có chắc chắn muốn xóa");
                b.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(Controller.ktXoa(bookList.get(positon).getId()))
                        {
                            Controller.delete(bookList.get(positon).getId());
                            bookList.remove(bookList.get(positon));

                            Intent intent = new Intent(v.getContext(), MainActivity.class);
                            context.startActivity(intent);
                        }
                        else{
                            Toast.makeText(v.getContext(), "Không xóa được", Toast.LENGTH_LONG).show();
                        }
                    }
                });

                b.setNegativeButton("Không đồng ý", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog al = b.create();
                al.show();
            }
        });
        return convertView;
    }
}
