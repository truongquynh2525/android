package com.example.thiguaky.src.Controller.Ticket.custom;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.Ticket.AddActivity;
import com.example.thiguaky.src.Controller.Ticket.TicketActivity;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Ticket;

import java.util.List;

public class MyBookTicketAdapter extends BaseAdapter {
    private  Context context;
    private List<Book> books;

    public MyBookTicketAdapter(Context context, List<Book> objects) {
        this.context = context;
        this.books = objects;

    }

    @Override
    public int getCount() {
        return books.size();
    }

    @Override
    public Book getItem(int position) {
        return books.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewBook;
        if (convertView == null) {
            viewBook = View.inflate(parent.getContext(), R.layout.ticket_book_dropdown_list_row, null);
        } else viewBook = convertView;
        TextView txtBook = viewBook.findViewById(R.id.textBookTicketOption);
        txtBook.setText(books.get(position).getName());
        CheckBox checkBox = viewBook.findViewById(R.id.checkboxBookTicket);

        checkBox.setText(String.valueOf(books.get(position).getId()));
        if(TicketActivity.isUpdate)
        {
            for(Integer ckId : AddActivity.booksTicketCheck){
                if(ckId == books.get(position).getId()){
                    checkBox.setChecked(true);
                    break;
                }
            }
        }
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    AddActivity.booksTicketCheck.add(Integer.parseInt(checkBox.getText().toString()));
                }else{
                    int vtRmove = -1;
                    int bookRemovId = Integer.parseInt(checkBox.getText().toString());
                    for(int i = 0; i<AddActivity.booksTicketCheck.size();i++){
                        if(bookRemovId == AddActivity.booksTicketCheck.get(i)){
                            vtRmove = i;
                            break;
                        }
                    }
                    if(vtRmove != -1 ){
                        AddActivity.booksTicketCheck.remove(vtRmove);
                    }

                }
            }
        });
        return viewBook;
    }

}
