package com.example.thiguaky.src.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.Librarian.LibrarianActivity;

import java.sql.Connection;

public class LoginActivity extends AppCompatActivity {
    EditText Username, Password, Register, Forgot;
    Button btnLogin, btnExit;

    ConnectToDB con;
    Connection conn;

    public static int libID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        View v = (View)findViewById(R.id.loginImg);
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_translate);
        v.setAnimation(animScale);
        con = new ConnectToDB();
        //setActionBar();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Login"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
       // actionBar.setLogo(R.mipmap.ic_launcher);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
        setEvent();
    }

    private void setActionBar() {

    }

    private void setControl() {
        //gan thong tin
        Username = (EditText) findViewById(R.id.username);
        Password = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnExit  = (Button) findViewById(R.id.btnExit);
    }

    private void setEvent() {
        Animation animAlpha = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);

        //btn dang nhap click
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.startAnimation(animAlpha);

                conn = con.conclass();
                if (con != null) {
                    System.out.println("click btn login");
                    libID = LibrarianActivity.checkLogin(conn, Username.getText().toString(), Password.getText().toString());
                    if (libID == 0) {
                        Toast.makeText(LoginActivity.this, "Tài khoản hoặc mật khẩu không đúng", Toast.LENGTH_LONG).show();
                        Username.requestFocus();
                    } else {

                        Toast.makeText(LoginActivity.this, "Đăng nhập thành công", Toast.LENGTH_LONG).show();
                        try {
                            conn.close();
                        }catch (Exception ex) {

                        }
                        Intent intent = new Intent(v.getContext(), HomeActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }
}