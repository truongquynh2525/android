package com.example.thiguaky.src.Controller.Reader;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Models.Reader;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class ReaderActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{
    Button add;
    GridView listReader;
    ListView listview;
    ListViewAdapter adpt;
    ConnectToDB con;
    Connection conn;
    Reader[] list;
    ArrayList<Reader> arraylist = new ArrayList<Reader>();
    SearchView search;

    public static void insert(Connection conn, String readerHo, String readerName, java.sql.Date birthday, int sex, String address) {
        String sql = "INSERT INTO DocGia VALUES (?, ?, ?, ?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, readerHo);
            stmt.setString(2, readerName);
            stmt.setDate(3, birthday);
            stmt.setInt(4, sex);
            stmt.setString(5, address);
            stmt.executeUpdate();

            stmt.close();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void update(Connection conn, int readerID, String readerHo, String readerName, Date birthday, int sex, String address) {
        String sql = "UPDATE DocGia SET ReaderHo = ?, ReaderTen = ?, BirthDay = ?, Sex = ?, Address = ? WHERE ReaderID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, readerHo);
            stmt.setString(2, readerName);
            stmt.setDate(3, birthday);
            stmt.setInt(4, sex);
            stmt.setString(5, address);
            stmt.setInt(6, readerID);
            stmt.executeUpdate();
            stmt.close();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public static void delete(Connection conn, int readerID) {
        String sql = "DELETE FROM DocGia WHERE ReaderID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, readerID);
            stmt.executeUpdate();
            stmt.close();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public int getLength() {
        int i = 0;
        String sql = "SELECT * FROM DocGia";
        try {
            conn = con.conclass();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                i++;
            }
            conn.close();
            stmt.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return i;
    }

    public int getLengthWithName(String name) {
        int i = 0;
        String sql = "SELECT * FROM DocGia WHERE ReaderTen LIKE '" + name + "%'";
        try {
            conn = con.conclass();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                i++;
            }
            conn.close();
            stmt.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return i;
    }

    public void getDataReader(Reader[] list) {
        int i = 0;
        String sql = "SELECT * FROM DocGia";
        try {
            conn = con.conclass();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Reader temp = new Reader();
                temp.setReaderID(rs.getInt("ReaderID"));
                temp.setReaderHo(rs.getString("ReaderHo"));
                temp.setReaderName(rs.getString("ReaderTen"));
                temp.setBirthDay(rs.getDate("BirthDay"));
                temp.setSex(rs.getInt("Sex"));
                temp.setAddress(rs.getString("Address"));
                list[i] = temp;
                i++;
            }
            conn.close();
            stmt.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public void getDataReaderWithName(Reader[] list, String name) {
        int i = 0;
        String sql = "SELECT * FROM DocGia WHERE ReaderTen LIKE '" + name + "%'";
        try {
            conn = con.conclass();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Reader temp = new Reader();
                temp.setReaderID(rs.getInt("ReaderID"));
                temp.setReaderHo(rs.getString("ReaderHo"));
                temp.setReaderName(rs.getString("ReaderTen"));
                temp.setBirthDay(rs.getDate("BirthDay"));
                temp.setSex(rs.getInt("Sex"));
                temp.setAddress(rs.getString("Address"));
                list[i] = temp;
                i++;
            }
            conn.close();
            stmt.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reader);
        View v = (View)findViewById(R.id.readerImg);
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
        v.setAnimation(animScale);
        con = new ConnectToDB();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Danh sach Doc gia"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
     //   actionBar.setLogo(R.mipmap.ic_launcher);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
        setDisplay();
        setEvent();
    }

    public void setControl() {
        add = (Button) findViewById(R.id.btnAdd);
        listReader = (GridView) findViewById(R.id.listReader);
        search = (SearchView) findViewById(R.id.search);
        listview = (ListView) findViewById(R.id.listview);
    }

    private void setDisplay() {
        int i = getLength();
        list = new Reader[i];
        getDataReader(list);
        CustomReaderAdapter adapter = new CustomReaderAdapter(this, R.layout.layout_reader, list);
        listReader.setAdapter(adapter);
    }

    boolean isOpen = false;
    private void displaySearch() {
        for(int j = 0; j < list.length; j++) {
            arraylist.add(list[j]);
        }
        adpt = new ListViewAdapter(this, arraylist);
        listview.setAdapter(adpt);
        search.setOnQueryTextListener(this);
    }

    public void setEvent() {
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddActivity.class);
                startActivity(intent);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isOpen) {
                    isOpen = true;
                    displaySearch();
                }
                else {
                    isOpen = false;
                    arraylist = new ArrayList<>();
                    listview.setAdapter(null);
                }
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String text = newText;
        adpt.filter(text);
        int i = getLengthWithName(text);
        list = new Reader[i];
        getDataReaderWithName(list, text);
        CustomReaderAdapter adapter = new CustomReaderAdapter(this, R.layout.layout_reader, list);
        listReader.setAdapter(adapter);
        return false;
    }
}