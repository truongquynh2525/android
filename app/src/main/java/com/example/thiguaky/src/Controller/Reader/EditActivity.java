package com.example.thiguaky.src.Controller.Reader;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;

public class EditActivity extends AppCompatActivity {
    EditText readerHo, readerName, diaChi;
    TextView readerid;
    CheckBox chxNam, chxNu;
    DatePicker date;
    Button btnAccept, btnExit;
    ConnectToDB con;
    Connection conn;
    int ID = 0, sex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_reader);
        String id = getIntent().getStringExtra("ID");
        ID = Integer.parseInt(id);
        con = new ConnectToDB();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
        setData();
        setEvent();
    }

    private void setControl() {
        readerid = (TextView) findViewById(R.id.edtreaderId);
        readerHo = (EditText) findViewById(R.id.edtreaderHo);
        readerName = (EditText) findViewById(R.id.edtreaderName);
        diaChi = (EditText) findViewById(R.id.edtreaderAddress);
        chxNam = (CheckBox) findViewById(R.id.edtchxNam);
        chxNu = (CheckBox) findViewById(R.id.edtchxNu);
        date = (DatePicker) findViewById(R.id.edtdate);
        btnAccept = (Button) findViewById(R.id.btnAccept);
        btnExit = (Button) findViewById(R.id.btnExit);
    }

    private void setData() {
        String sql = "SELECT * FROM DocGia WHERE ReaderID = " + ID;
        System.out.println(sql);
        try {
            conn = con.conclass();
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                readerid.setText(ID + "");
                readerid.setEnabled(false);
                readerHo.setText(rs.getString(2));
                readerName.setText(rs.getString(3));
                date.updateDate(rs.getDate(4).getDate(), rs.getDate(4).getMonth(), rs.getDate(4).getYear());
                if(rs.getInt(5) == 1) {
                    chxNam.setChecked(true);
                }
                else {
                    chxNu.setChecked(false);
                }
                diaChi.setText(rs.getString(6));
            }
            conn.close();
            stmt.close();
        } catch (Exception ex) {
            System.err.println(ex);
        }
    }

    private void setEvent() {
        if(chxNam.isChecked()) {
            sex = 1;
            chxNu.setChecked(false);
        }
        else {
            sex = 0;
            chxNam.setChecked(false);
        }
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                conn = con.conclass();
                long dateTime = date.getCalendarView().getDate();
                Date time = new Date(dateTime);
                System.out.println(readerHo.getText().toString());
                System.out.println(readerName.getText().toString());
                System.out.println(time);
                System.out.println(sex);
                System.out.println(diaChi.getText().toString());
                ReaderActivity.update(conn, ID, readerHo.getText().toString(), readerName.getText().toString(), time, sex, diaChi.getText().toString());
                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                startActivity(intent);
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                startActivity(intent);
            }
        });
    }
}