package com.example.thiguaky.src.Controller.Book;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.GridView;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.Book.Custom.CustomBookAdapter;
import com.example.thiguaky.src.Controller.Ticket.dao.TicketDao;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Ticket;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    Button btnAdd;
    ConnectToDB con;
    SearchView searchView;
    CustomBookAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        View v = (View)findViewById(R.id.bookImg);
        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
        v.setAnimation(animScale);
        Controller bookController=new Controller();

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Danh sách sách"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
       // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
    }

    private void setControl()
    {
        this.gridView = (GridView)findViewById(R.id.gridView);
        this.btnAdd=(Button)findViewById(R.id.btnAdd);
        this.searchView=(SearchView)findViewById(R.id.searchView);

        adapter=new CustomBookAdapter(this,R.layout.layout_book,Controller.loadData());
        gridView.setAdapter(adapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddActivity.class);
                startActivity(intent);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                try {
                    adapter.setBookList(Controller.filter(text));
                    gridView.setAdapter(adapter);
                }catch (Exception ex){
                   System.out.println(ex.getMessage());
                }
                return false;
            }
        });
    }
}