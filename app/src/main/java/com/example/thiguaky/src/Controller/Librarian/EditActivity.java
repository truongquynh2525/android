package com.example.thiguaky.src.Controller.Librarian;

import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;

public class EditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_librarian);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
    }
}