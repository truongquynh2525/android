package com.example.thiguaky.src.Controller.Ticket.dao;

import android.util.Log;

import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Reader;
import com.example.thiguaky.src.Models.Ticket;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TicketDao {
    public static List<Ticket> filter(Connection conn,int query){
        List<Ticket> list = new ArrayList<>();
        String sql = "SELECT * FROM PhieuMuon where ReaderId = ? ORDER BY Times DESC";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,query);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.setTicketID(resultSet.getInt(1));
                ticket.setLibID(resultSet.getInt(2));
                ticket.setReaderID(resultSet.getInt(3));
                ticket.setTimes(resultSet.getDate(4));
                ticket.setNotes(resultSet.getString(5));
                list.add(ticket);
                Log.println(Log.INFO,null,ticket.toString());
            }
            resultSet.close();
            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return list;
    }
    public static void insertCtpm(Connection conn, int ticketId,int bookId){
        String sql = "INSERT INTO CTPM VALUES (? ,?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, (int) ticketId);
            stmt.setInt(2, bookId);
            stmt.setInt(3, 0);
            stmt.executeUpdate();
            stmt.close();
            conn.close();
        }catch(Exception ex){
            Log.println(Log.ERROR,null,ex.getMessage());
        }
    }

    public static int insert(Connection conn, int libID, int readerID, String time, String notes) {
        long lastId = -1;
        String sql = "INSERT INTO PhieuMuon VALUES (? ,? ,? ,?)";

        try {
            PreparedStatement stmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            stmt.setInt(1, libID);
            stmt.setInt(2, readerID);
            stmt.setDate(3, Date.valueOf(time));
            stmt.setString(4, notes);

            int affectedRows =  stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Creating user failed, no rows affected.");
            }
            ResultSet generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                lastId = generatedKeys.getLong(1);
            }
            else {
                throw new SQLException("Creating user failed, no ID obtained.");
            }


            generatedKeys.close();
            stmt.close();
            conn.close();

        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return (int) lastId;
    }

    public static void update(Connection conn, int ticketID, int libID, int readerID, String time, String notes) {
        String sql = "UPDATE PhieuMuon SET LibID = ?, ReaderID = ?, Times = ?, Notes = ? WHERE TicketID = ?";
        System.out.println(notes +"|"+readerID+"|"+ libID);
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, libID);
            stmt.setInt(2, readerID);
            stmt.setDate(3, Date.valueOf(time) );
            stmt.setString(4, notes);
            stmt.setInt(5, ticketID);
            stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
    }
    public static void deleteCTPM(Connection conn,int ticketID){
        String sql = "DELETE FROM CTPM WHERE TicketID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ticketID);
            stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
    }

    public static void delete(Connection conn, int ticketID) {
        String sql = "DELETE FROM CTPM WHERE TicketID = ?";
        String sql2 = "DELETE FROM PhieuMuon WHERE TicketID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ticketID);
            stmt.executeUpdate();

            stmt = conn.prepareStatement(sql2);
            stmt.setInt(1, ticketID);
            stmt.executeUpdate();

            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
    }

    public static List<Ticket> fecthTicket(Connection conn) {
        List<Ticket> list = new ArrayList<>();
        String sql = "SELECT * FROM PhieuMuon ORDER BY TicketID DESC";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.setTicketID(resultSet.getInt(1));
                ticket.setLibID(resultSet.getInt(2));
                ticket.setReaderID(resultSet.getInt(3));
                ticket.setTimes(resultSet.getDate(4));
                ticket.setNotes(resultSet.getString(5));
                list.add(ticket);
                Log.println(Log.INFO,null,ticket.toString());
            }
            resultSet.close();
            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return list;
    }

    public static List<Book> fetchSach(Connection conn) {
        List<Book> list = new ArrayList<>();
        String sql = "SELECT * FROM Sach ORDER BY id DESC";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("id"));
                book.setName(resultSet.getString("name"));
                book.setPrice(resultSet.getFloat("price"));
                book.setImg(resultSet.getString("img"));
                list.add(book);
            }
            resultSet.close();
            stmt.close();
            conn.close();
        } catch (Exception ex) {
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return list;
    }
    public static List<Integer> getBookByTicketId(Connection conn, int ticketID){
        List<Integer> bookIds = new ArrayList<>();
        String sql = "SELECT BookID FROM CTPM where TicketID = ?";
        try{
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ticketID);
            ResultSet resultSet = stmt.executeQuery();
            while(resultSet.next()){
                bookIds.add(resultSet.getInt("BookID"));
            }
            resultSet.close();
            stmt.close();
            conn.close();
        }catch (Exception ex){
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return bookIds;
    }
    public static List<Reader> fetchReader(Connection conn){
        List<Reader> readers = new ArrayList<>();
        String sql = "select * from DocGia";
        try{
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()){
                Reader reader = new Reader();
                reader.setReaderID(resultSet.getInt("ReaderID"));
                reader.setReaderHo(resultSet.getString("ReaderHo"));
                reader.setReaderName(resultSet.getString("ReaderTen"));
                readers.add(reader);
            }
            resultSet.close();
            stmt.close();
            conn.close();
        }catch (Exception ex){
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return readers;
    }
    public static List<Reader> filterReader(Connection conn,String query){
        List<Reader> readers = new ArrayList<>();
        String sql = "select * from DocGia as d where d.ReaderTen like ?";
        try{
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,'%'+ query + '%');
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()){
                Reader reader = new Reader();
                reader.setReaderID(resultSet.getInt("ReaderID"));
                reader.setReaderHo(resultSet.getString("ReaderHo"));
                reader.setReaderName(resultSet.getString("ReaderTen"));
                readers.add(reader);
                System.out.println(reader);
            }
            resultSet.close();
            stmt.close();
            conn.close();
        }catch (Exception ex){
            Log.println(Log.ERROR,null,ex.getMessage());
        }
        return readers;
    }
}
