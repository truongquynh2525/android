package com.example.thiguaky.src.Controller.ReturnBook;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.SearchView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.ReturnBook.Custom.CustomReaderAdapter;
import com.example.thiguaky.src.Models.Reader;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    SearchView searchView;
    CustomReaderAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_borrow_reader);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Trả sách"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setControl();
        setEvent();
    }

    private void setControl()
    {
        this.gridView = (GridView)findViewById(R.id.gridView);
        this.searchView=(SearchView)findViewById(R.id.searchView);

        Controller bookController=new Controller();

        adapter=new CustomReaderAdapter(this,R.layout.layout_borrow_reader,Controller.loadListReaderBorrow());
        gridView.setAdapter(adapter);
    }

    private void setEvent(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String text) {
                try {
                    adapter.setReaders(Controller.filter(text));
                    gridView.setAdapter(adapter);
                }catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
                return false;
            }
        });
    }
}
