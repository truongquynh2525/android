package com.example.thiguaky.src.Controller.Ticket;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.Ticket.custom.MyListViewAdapter;
import com.example.thiguaky.src.Controller.Ticket.dao.TicketDao;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Ticket;

import java.sql.Connection;
import java.util.List;

public class TicketActivity extends AppCompatActivity {
    ListView listTicket;
    Button btnThemPhieuMuon;
    SearchView searchViewTicket;
    MyListViewAdapter mylist;
    ImageView ticketImage;

    public static boolean isUpdate;

    ConnectToDB con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ticket);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Mượn sách"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        con = new ConnectToDB();
        setControl();
        setEvent();
        isUpdate = false;
    }

    public void setControl() {
        loadListTicket();
        btnThemPhieuMuon = findViewById(R.id.btnThemTicket);
        searchViewTicket = findViewById(R.id.search_ticket);
        ticketImage = findViewById(R.id.ticket_image_list);

        Animation animScale = AnimationUtils.loadAnimation(this, R.anim.anim_scale);
        ticketImage.setAnimation(animScale);

    }

    public void setEvent() {
        btnThemPhieuMuon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isUpdate = false;
                Intent intent = new Intent(v.getContext(), AddActivity.class);
                startActivity(intent);
            }
        });
        searchViewTicket.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    if(newText.length()>0){
                        System.out.println(newText);
                        Connection conn = con.conclass();
                        List<Ticket> filters = TicketDao.filter(conn,Integer.valueOf(newText));
                        mylist.setTickets(filters);
                        listTicket.setAdapter(mylist);
                    }else{
                        loadListTicket();
                    }
                }catch (Exception ex){
                    Toast.makeText(getActivity(),"Input valid ! Input Number",
                            Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });
    }

    public void loadListTicket() {
        listTicket = findViewById(R.id.listTicket);
        Connection conn = con.conclass();
        List<Ticket> tickets = TicketDao.fecthTicket(conn);
        mylist = new MyListViewAdapter( this,tickets);
        listTicket.setAdapter(mylist);

    }
    public TicketActivity getActivity(){
        return this;
    }
}