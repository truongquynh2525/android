package com.example.thiguaky.src.Controller.Ticket.custom;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.Ticket.AddActivity;
import com.example.thiguaky.src.Controller.Ticket.TicketActivity;
import com.example.thiguaky.src.Controller.Ticket.dao.TicketDao;
import com.example.thiguaky.src.Models.Ticket;

import java.sql.Connection;
import java.util.List;

public class MyListViewAdapter extends BaseAdapter {
//    private Context context;
    private List<Ticket> tickets;
    Button btnXoaPhieuMuon;
    Button btnUpdatePhieuMuon;

    ConnectToDB con;
    Context context;

    public MyListViewAdapter(Context context,List<Ticket> tickets) {
//        this.context = context;
        this.tickets = tickets;
        this.con = new ConnectToDB();
        this.context = context;
    }

    @Override
    public int getCount() {
        return tickets.size();
    }

    @Override
    public Object getItem(int position) {
        return tickets.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tickets.get(position).getTicketID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewProduct;
        if (convertView == null) {
            viewProduct = View.inflate(parent.getContext(), R.layout.ticket_item_list, null);
        } else viewProduct = convertView;

        TextView docgiaItem = (TextView)  viewProduct.findViewById(R.id.docGiaItemTicket);
        TextView dateItem = (TextView)  viewProduct.findViewById(R.id.dateItemTicket);
        TextView idTicketItem = (TextView)  viewProduct.findViewById(R.id.idItemTicket);
        ImageView imageView =(ImageView) viewProduct.findViewById(R.id.imageTicket);

        Ticket ticket = tickets.get(position);
        idTicketItem.setText(String.format("Số phiếu : %d" , ticket.getTicketID()));
        docgiaItem.setText(String.format("Độc giả : %d",ticket.getReaderID()));
        dateItem.setText(String.format("Ngày mượn : %s",tickets.get(position).getTimes().toString()));
        imageView.setImageResource(R.drawable.ticket);

        btnXoaPhieuMuon = viewProduct.findViewById(R.id.btnDeleteTicket);
        btnXoaPhieuMuon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Ticket ticket = tickets.get(position);
//                Connection conn = con.conclass();
//                TicketDao.delete(conn,ticket.getTicketID());
//                Toast.makeText(context, "Xóa thành công", Toast.LENGTH_SHORT).show();
//                Intent myIntent = new Intent(context, TicketActivity.class);
//                context.startActivity(myIntent);
            }
        });

        btnUpdatePhieuMuon = viewProduct.findViewById(R.id.btnUpdateTicket);
        btnUpdatePhieuMuon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddActivity.ticketSelectUpdate = tickets.get(position);
                TicketActivity.isUpdate = true;
                Intent intent = new Intent(v.getContext(), AddActivity.class);
                context.startActivity(intent);
            }
        });

        return viewProduct;

    };

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }
}
