package com.example.thiguaky.src.Controller.ReturnBook.Custom;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.ReturnBook.Controller;
import com.example.thiguaky.src.Controller.ReturnBook.ReturnActivity;
import com.example.thiguaky.src.Models.ReturnBook;

import java.util.ArrayList;

public class CustomReturnBookAdapter extends ArrayAdapter {
    Context context;
    ArrayList<ReturnBook> bookList=new ArrayList<>();
    int layoutID;

    public CustomReturnBookAdapter(@NonNull Context context, int layoutID, ArrayList<ReturnBook> bookList) {
        super(context, layoutID);
        this.context = context;
        this.bookList = bookList;
        this.layoutID = layoutID;
    }

    @Override
    public int getCount()
    {
        return bookList.size();
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater.from(context));
        convertView = inflater.inflate(layoutID, null);
        TextView ticket = (TextView) convertView.findViewById(R.id.text_ticket_item);
        TextView name = (TextView) convertView.findViewById(R.id.text_name_book_item);
        TextView date = (TextView) convertView.findViewById(R.id.text_date_item);
        ImageView img = (ImageView) convertView.findViewById(R.id.text_img_book_item);
        Button btnReturnBook = (Button) convertView.findViewById(R.id.btnReturnBook);

        String imgUrl = bookList.get(positon).getImg();

        ticket.setText(bookList.get(positon).getIdBorrow() + "");
        name.setText(bookList.get(positon).getName());
        date.setText(bookList.get(positon).getDate().toString());

        try {
            img.setImageURI(Uri.parse(bookList.get(positon).getImg()));
            if(img.getDrawable() == null)
            {
                img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
            }
        }catch (Exception exception)
        {
            img.setImageDrawable(context.getResources().getDrawable(context.getResources().getIdentifier("drawable/book", "drawable", context.getPackageName())));
        }

        btnReturnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder b = new AlertDialog.Builder(context);

                b.setTitle("Xác nhận");
                b.setMessage("Bạn có chắc chắn muốn trả sách này");
                b.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Controller.updateBorrowBook(1,bookList.get(positon).getIdBorrow(),bookList.get(positon).getIdBook());
                        bookList.remove(bookList.get(positon));
                        Intent intent = new Intent(v.getContext(), ReturnActivity.class);
                        context.startActivity(intent);
                        Toast.makeText(context, "Thành công", Toast.LENGTH_LONG).show();
                    }
                });

                b.setNegativeButton("Không đồng ý", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog al = b.create();
                al.show();
            }
        });
        return convertView;
    }
}
