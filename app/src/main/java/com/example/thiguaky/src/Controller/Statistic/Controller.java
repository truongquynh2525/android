package com.example.thiguaky.src.Controller.Statistic;

import android.graphics.Color;

import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Models.Book;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class Controller {
    public static Connection conn=null;

    public Controller()
    {
        conn=new ConnectToDB().conclass();
    }

    public static ArrayList<Book> statisticBookBorrowMax() {
        ArrayList<Book> arrayList=new ArrayList<>();
        String sql = "SELECT Sach.name,Sach.img,COUNT(TicketID) AS SoLan\n" +
                "  FROM CTPM\n" +
                "  JOIN Sach ON CTPM.BookID = Sach.id\n" +
                "  GROUP BY Sach.name,Sach.img\n" +
                "  ORDER BY SoLan DESC";
        try {
            int i=1;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Book book=new Book(i,rs.getString("name"),
                        rs.getFloat("SoLan"),rs.getString("img"));
                arrayList.add(book);
                i++;
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return  arrayList;
    }

    public static int[] statisticBookBorrowByMount() {
        int[] array=new int[12];
        for(int i=0;i<12;i++)
        {
            array[i]=0;
        }

        String sql = "SELECT MONTH(Times) Thang, COUNT(*) AS 'SoLuong'\n" +
                "\tFROM PhieuMuon\n" +
                "\tJOIN CTPM ON PhieuMuon.TicketID = CTPM.TicketID\n" +
                "\tGROUP BY MONTH(Times)\n" +
                "\tORDER BY MONTH(Times)";
        try {

            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                array[rs.getInt("Thang")]=rs.getInt("SoLuong");
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return  array;
    }

    public static DataSet dataChart(int[] data, String str) {

        LineData d = new LineData();

        ArrayList<Entry> entries = new ArrayList<Entry>();

        for (int index = 0; index < data.length; index++) {
            entries.add(new Entry(index, data[index]));
        }

        LineDataSet set = new LineDataSet(entries, str);
        set.setColor(Color.BLUE);
        set.setLineWidth(2.5f);
        set.setCircleColor(Color.BLUE);
        set.setCircleRadius(5f);
        set.setFillColor(Color.BLUE);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setDrawValues(true);
        set.setValueTextSize(10f);
        set.setValueTextColor(Color.BLUE);

        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        d.addDataSet(set);

        return set;
    }

    public static ArrayList<Book> get10BookByMount(int n) {
        ArrayList<Book> arrayList=new ArrayList<>();

        String sql = "SELECT Top 10 Sach.id,Sach.name,COUNT(CTPM.TicketID) AS SoLg\n" +
                "\t  FROM PhieuMuon\n" +
                "\t  JOIN CTPM ON PhieuMuon.TicketID = CTPM.TicketID\n" +
                "\t  JOIN Sach ON Sach.id = CTPM.BookID\n" +
                "\t  WHERE MONTH(PhieuMuon.Times) ="+n+"\n"+
                "\t  GROUP BY Sach.id,Sach.name\n" +
                "\t  ORDER BY SoLg DESC";
        try {

            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            int i=1;
            while (rs.next()) {
                Book book=new Book(i,rs.getString("name"),
                        rs.getFloat("SoLg"),"");
                arrayList.add(book);
                i++;
            }
            rs.close();
            ps.close();

        } catch (Exception ex) {
            System.err.println(ex);
        }

        return  arrayList;
    }
}
