package com.example.thiguaky.src.Models;

public class Librarian {
    private int LibID;
    private String LibName;
    private String SDT;
    private String username;
    private String password;

    public int getLibID() {
        return LibID;
    }

    public void setLibID(int libID) {
        LibID = libID;
    }

    public String getLibName() {
        return LibName;
    }

    public void setLibName(String libName) {
        LibName = libName;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
