package com.example.thiguaky.src.Controller.ReturnBook.Custom;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.ReturnBook.ReturnActivity;
import com.example.thiguaky.src.Models.Reader;

import java.util.ArrayList;

public class CustomReaderAdapter extends ArrayAdapter {
    Context context;
    ArrayList<Reader> readers =new ArrayList<>();
    int layoutID;

    public CustomReaderAdapter(@NonNull Context context, int layoutID, ArrayList<Reader> bookList) {
        super(context, layoutID);
        this.context = context;
        this.readers = bookList;
        this.layoutID = layoutID;
    }

    public ArrayList<Reader> getReaders() {
        return readers;
    }

    public void setReaders(ArrayList<Reader> readers) {
        this.readers = readers;
    }

    @Override
    public int getCount()
    {
        return readers.size();
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater.from(context));
        convertView = inflater.inflate(layoutID, null);
        TextView id = (TextView) convertView.findViewById(R.id.text_id_reader);
        TextView name = (TextView) convertView.findViewById(R.id.text_name_reader);
        TextView date = (TextView) convertView.findViewById(R.id.text_date_reader);
        TextView address = (TextView) convertView.findViewById(R.id.text_address_reader);
        Button btnReturnBook = (Button) convertView.findViewById(R.id.btnReturnBook);

        id.setText(readers.get(positon).getReaderID() + "");
        name.setText(readers.get(positon).getReaderHo()+" "+ readers.get(positon).getReaderName());
        date.setText(readers.get(positon).getBirthDay().toString());
        address.setText(readers.get(positon).getAddress());

        btnReturnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ReturnActivity.class);
                intent.putExtra("READER_ID", readers.get(positon).getReaderID()+"");
                context.startActivity(intent);
            }
        });

        return convertView;
    }
}
