package com.example.thiguaky.src.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Controller.Librarian.LibrarianActivity;
import com.example.thiguaky.src.Controller.Reader.ReaderActivity;
import com.example.thiguaky.src.Controller.ReturnBook.MainActivity;
import com.example.thiguaky.src.Controller.Ticket.TicketActivity;


public class HomeActivity extends AppCompatActivity {
    ImageButton btnLib, btnReader, btnBook, btnResend, btnBorrow,btnStatistic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        View v = (View)findViewById(R.id.homeImg);
        Animation animRotate = AnimationUtils.loadAnimation(this, R.anim.anim_rotate);
        v.setAnimation(animRotate);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Home Main"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
       // actionBar.setLogo(R.mipmap.ic_launcher);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);
        setControl();
        setEvent();
    }

    private void setControl() {
        btnLib = (ImageButton) findViewById(R.id.btnLib);
        btnReader = (ImageButton) findViewById(R.id.btnReader);
        btnBook = (ImageButton) findViewById(R.id.btnBook);
        btnResend = (ImageButton) findViewById(R.id.btnResend);
        btnBorrow = (ImageButton) findViewById(R.id.btnBorrow);
        btnStatistic= (ImageButton) findViewById(R.id.btnStatistic);
    }

    private void setEvent() {
        Animation animAlpha = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);

        btnLib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), LibrarianActivity.class);
                startActivity(intent);
            }
        });
        btnReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                startActivity(intent);
            }
        });
        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), com.example.thiguaky.src.Controller.Book.MainActivity.class);
                startActivity(intent);
            }
        });
        btnResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        btnStatistic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), com.example.thiguaky.src.Controller.Statistic.MainActivity.class);
                startActivity(intent);
            }
        });
        btnBorrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setAnimation(animAlpha);
                Intent intent = new Intent(v.getContext(), TicketActivity.class);
                startActivity(intent);
            }
        });
    }
}