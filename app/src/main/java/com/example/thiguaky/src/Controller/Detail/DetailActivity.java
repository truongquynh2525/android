package com.example.thiguaky.src.Controller.Detail;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class DetailActivity extends AppCompatActivity {

    public void insert(Connection conn, int ticketID, int bookID) {
        String sql = "INSERT INTO CTPM VALUES (?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ticketID);
            stmt.setInt(2, bookID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }

    public void delete(Connection conn, int ticketID) {
        String sql = "DELETE FROM CTPM WHERE TicketID = ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, ticketID);
            stmt.executeUpdate();
        }
        catch (Exception ex) {
            System.err.println(ex);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
    }
}