package com.example.thiguaky.src.Controller.Ticket;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Controller.LoginActivity;
import com.example.thiguaky.src.Controller.Ticket.custom.MyBookTicketAdapter;
import com.example.thiguaky.src.Controller.Ticket.dao.TicketDao;
import com.example.thiguaky.src.Models.Book;
import com.example.thiguaky.src.Models.Reader;
import com.example.thiguaky.src.Models.Ticket;
import com.toptoche.searchablespinnerlibrary.SearchableListDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AddActivity extends AppCompatActivity {
    TextView readerID;
    EditText notes;
    TextView dateView;
    ListView listBookInAddTicket;
    Button btnAccept;
    Button btnHuy;
    SearchableSpinner spinnerReader;

    List<Book> booksTicket;
    List<Reader> readersTicket;
    ConnectToDB connect;
    ArrayAdapter<Reader> readerAdapter;
    MyBookTicketAdapter sachAdapter;
    Reader readerSelected;
    public static Ticket ticketSelectUpdate;
    public static List<Integer> booksTicketCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Quay lại"); //Thiết lập tiêu đề nếu muốn
        actionBar.setDisplayShowHomeEnabled(true);
        // actionBar.setLogo(R.drawable.sachlogo);    //Icon muốn hiện thị
        actionBar.setDisplayUseLogoEnabled(true);

        setContentView(R.layout.ticket_add);
        connect = new ConnectToDB();
        setControl();
        setEvent();
    }

    public void setControl() {
        booksTicketCheck = new ArrayList<>();
        Date d = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");


        btnAccept = findViewById(R.id.btnSaveTicket);
        btnHuy = findViewById(R.id.btnExitTicket);
        readerID = findViewById(R.id.ticket_reader_id);
        notes = findViewById(R.id.notes);
        dateView = findViewById(R.id.ticket_date_view);
        listBookInAddTicket = findViewById(R.id.listviewBookTicket);
        spinnerReader = findViewById(R.id.ticket_spinner_docgia);

        loadListBook();
        loadSpinnerReader();

        if (TicketActivity.isUpdate) {
            for(int i =0; i<readersTicket.size();i++){
                if( readersTicket.get(i).getReaderID() == ticketSelectUpdate.getReaderID()){
                    spinnerReader.setSelection(i);
                    readerSelected = (Reader) spinnerReader.getSelectedItem();
                    break;
                }
            }
            readerID.setText(String.valueOf(readerSelected.getReaderID()));
            notes.setText(ticketSelectUpdate.getNotes());
            dateView.setText(format.format(d));
            booksTicketCheck = TicketDao.getBookByTicketId(connect.conclass(), ticketSelectUpdate.getTicketID());
        } else {
            dateView.setText(format.format(d));
        }


    }

    public void setEvent() {
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int readerid = Integer.parseInt(readerID.getText().toString());
                int libId = LoginActivity.libID;
                if (TicketActivity.isUpdate) {
                    Connection conn = connect.conclass();
                    TicketDao.update(conn, ticketSelectUpdate.getTicketID(), libId,readerid , dateView.getText().toString(), notes.getText().toString());
                    conn = connect.conclass();
                    TicketDao.deleteCTPM(conn, ticketSelectUpdate.getTicketID());
                    for (Integer item : booksTicketCheck) {
                        Connection conn2 = connect.conclass();
                        TicketDao.insertCtpm(conn2, ticketSelectUpdate.getTicketID(), item);
                    }
                    Intent intent = new Intent(v.getContext(), TicketActivity.class);
                    startActivity(intent);
                } else {
                    if (booksTicketCheck.size() > 0) {
                        int lastId = TicketDao.insert(connect.conclass()
                                , libId
                                , readerid
                                , dateView.getText().toString()
                                , notes.getText().toString());

                        for (Integer item : booksTicketCheck) {
                            TicketDao.insertCtpm(connect.conclass(), lastId, item);
                        }

                        Intent intent = new Intent(v.getContext(), TicketActivity.class);
                        startActivity(intent);

                    }
                }
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TicketActivity.class);
                startActivity(intent);
            }
        });
        spinnerReader.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                readerSelected = (Reader) spinnerReader.getSelectedItem();
                readerID = getAddActivity().findViewById(R.id.ticket_reader_id);
                readerID.setText(String.valueOf(readerSelected.getReaderID()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void loadSpinnerReader(){
        readersTicket = TicketDao.fetchReader(connect.conclass());
        readerAdapter = new ArrayAdapter(this,R.layout.ticket_spinner_reader,R.id.ticket_spinner_docgia_item,readersTicket);
        spinnerReader.setAdapter(readerAdapter);
        spinnerReader.setTitle("Chọn độc giả");
    }
    public void loadListBook(){
        booksTicket = TicketDao.fetchSach(connect.conclass());
        sachAdapter = new MyBookTicketAdapter(this, booksTicket);
        listBookInAddTicket.setAdapter(sachAdapter);
    }
    public AddActivity getAddActivity(){
        return this;
    }

}