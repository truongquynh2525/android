package com.example.thiguaky.src;

import android.annotation.SuppressLint;
import android.os.StrictMode;
import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectToDB {
    Connection con;

    @SuppressLint("NewApi")
    public Connection conclass() {
        String ip = "192.168.1.5", port = "1433", db = "Android", username = "sa", password = "123";

        StrictMode.ThreadPolicy a = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(a);
        String connectURL = null;

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            connectURL = "jdbc:jtds:sqlserver://" + ip + ":" + port + ";databasename=" + db + ";user=" + username + ";"+"password=" + password + ";";
            con = DriverManager.getConnection(connectURL);
            Log.e("Success   :", "Connect success");
        } catch (Exception e) {
            Log.e("Error :", e.getMessage());
        }
        return con;
    }
}
