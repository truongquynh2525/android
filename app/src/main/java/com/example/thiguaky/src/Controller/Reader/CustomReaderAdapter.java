package com.example.thiguaky.src.Controller.Reader;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.example.thiguaky.R;
import com.example.thiguaky.src.ConnectToDB;
import com.example.thiguaky.src.Models.Reader;

import java.sql.Connection;

public class CustomReaderAdapter extends ArrayAdapter {
    Context context;
    Reader listReader[];
    int layoutID;
    ConnectToDB con;
    Connection conn;

    public CustomReaderAdapter(@NonNull Context context, int layoutID, Reader[] listReader) {
        super(context, layoutID);
        this.context = context;
        this.listReader = listReader;
        this.layoutID = layoutID;
    }

    @Override
    public int getCount() {
        return listReader.length;
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater=(LayoutInflater.from(context));
        convertView=inflater.inflate(layoutID,null);
        TextView id=(TextView)convertView.findViewById(R.id.text_id_reader);
        TextView name=(TextView) convertView.findViewById(R.id.text_name_reader);
        TextView sex=(TextView)convertView.findViewById(R.id.text_sex_reader);
        TextView date=(TextView) convertView.findViewById(R.id.text_date_reader);
        TextView adr=(TextView) convertView.findViewById(R.id.text_address_reader);
        ImageButton btnEditReader = (ImageButton)convertView.findViewById(R.id.btnEditReader);
        ImageButton btnDeleteReader = (ImageButton)convertView.findViewById(R.id.btnDeleteReader);

        id.setText(listReader[positon].getReaderID() + "");
        name.setText(listReader[positon].getReaderHo() + " " + listReader[positon].getReaderName());
        if(listReader[positon].getSex() == 1) {
            sex.setText("Nam");
        }
        else {
            sex.setText("Nữ");
        }
        date.setText(listReader[positon].getBirthDay().toString());
        adr.setText(listReader[positon].getAddress());
        con = new ConnectToDB();
        conn = con.conclass();
        btnDeleteReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                builder1.setMessage("Dữ liệu sẽ mất vĩnh viễn?");
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Xác nhận",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ReaderActivity.delete(conn, listReader[positon].getReaderID());
                                Intent intent = new Intent(v.getContext(), ReaderActivity.class);
                                context.startActivity(intent);
                            }
                        });
                builder1.setNegativeButton(
                        "Hủy",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        btnEditReader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), EditActivity.class);
                intent.putExtra("ID", listReader[positon].getReaderID() + "");
                context.startActivity(intent);
            }
        });
        return convertView;
    }
}
