package com.example.thiguaky.src.Controller.Book;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thiguaky.R;
import com.example.thiguaky.src.Models.Book;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ListViewAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater inflater;
    private List<Book> bookList = null;
    private ArrayList<Book> arraylist;

    public ListViewAdapter(Context context, List<Book> animalNamesList) {
        mContext = context;
        this.bookList = animalNamesList;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Book>();
        this.arraylist.addAll(animalNamesList);
    }

    public class ViewHolder {
        TextView name;
    }

    @Override
    public int getCount() {
        return bookList.size();
    }

    @Override
    public Book getItem(int position) {
        return bookList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final com.example.thiguaky.src.Controller.Book.ListViewAdapter.ViewHolder holder;
        if (view == null) {
            holder = new com.example.thiguaky.src.Controller.Book.ListViewAdapter.ViewHolder();
            view = inflater.inflate(R.layout.reader_search, null);
            holder.name = (TextView) view.findViewById(R.id.name);
            view.setTag(holder);
        } else {
            holder = (com.example.thiguaky.src.Controller.Book.ListViewAdapter.ViewHolder) view.getTag();
        }
        System.out.println("view");
        // Set the results into TextViews
        holder.name.setText("Họ và tên: " + bookList.get(position).getName());
        return view;
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        bookList.clear();
        if (charText.length() == 0) {
            bookList.addAll(arraylist);
        } else {
            for (Book wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    bookList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
